import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { timeout, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

export class DataSasService {

  constructor(private http: HttpClient) {
  }
 // https://dsdgvamlweb.davivienda.loc:8343/SASStoredProcess/do?_program=%2FSystem%2FApplications%2FSAS+Compliance+Solutions%2FCompliance+Solutions+Svr%2FApplication+SAS+Code%2FPrueba_Campos


  getClientsCrm(url, reportKey) {
    return this.http.get(url + '/organizations/' + reportKey)
      .pipe(
        timeout(5000),
        map(res => res)
      )
      .toPromise()
  }
}
