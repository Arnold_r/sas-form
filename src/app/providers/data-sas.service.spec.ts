import { TestBed } from '@angular/core/testing';

import { DataSasService } from './data-sas.service';

describe('DataSasService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DataSasService = TestBed.get(DataSasService);
    expect(service).toBeTruthy();
  });
});
