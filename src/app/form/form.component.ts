import { Component, OnInit, ViewEncapsulation, Input } from '@angular/core';
import { Hero } from './data';
import { DataSasService } from '../providers/data-sas.service';

@Component({
  selector: 'custom-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss'],
  encapsulation: ViewEncapsulation.ShadowDom
})
export class FormComponent implements OnInit {

  @Input() originUrl: string;
  @Input() reportKey: string;

  public model: any = {
    instructionsNoFatca: ''
  }
  public profile = 'Investment';

  public customerName: any = [];

  public isCollapsed = false;
  public isDocumentationCollapsed = false;
  public isObservationsCollapsed = false;
  public isLegalEntityCollapsed = false;
  public isForeignCurrencyCollapsed = false;
  constructor(private generalService: DataSasService) {
  }

  ngOnInit() {
    this.generalService.getClientsCrm(this.originUrl, this.reportKey).then((_res) => {
      console.log(_res);
    }).catch((_err) => {
      console.log(_err);
    })
    console.log("url2", this.originUrl, this.reportKey);
  }

  onSubmit() {
    console.log("submit");
  }
}
