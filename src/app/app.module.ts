import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Injector } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { createCustomElement } from '@angular/elements';
import { HttpClientModule } from '@angular/common/http';

import { CollapseModule } from 'ngx-bootstrap/collapse';

import { ButtonComponent } from './button/button.component';
import { FormComponent } from './form/form.component';

@NgModule({
  declarations: [ButtonComponent, FormComponent],
  imports: [BrowserModule, HttpClientModule, FormsModule, CollapseModule.forRoot()],
  entryComponents: [ButtonComponent, FormComponent]
})
export class AppModule {
  constructor(private injector: Injector) {
    const customButton = createCustomElement(ButtonComponent, { injector });
    customElements.define('custom-button', customButton);

    const customForm = createCustomElement(FormComponent, { injector });
    customElements.define('custom-form', customForm);
  }

  ngDoBootstrap() { }
}